#!/bin/sh
set -e
echo "The application will start in ${JAVA_EXEC_SLEEP}s..." && sleep ${JAVA_EXEC_SLEEP}
exec java ${JAVA_OPTS} -Djava.security.egd=file:/dev/./urandom -jar "/app/app.jar" "$@"
