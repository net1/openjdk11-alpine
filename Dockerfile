FROM alpine:3.8
ARG version
LABEL description="Based upon keckelt/openjdk11-alpine" maintainer="Net1" version="$version"

ENV LANG C.UTF-8

ARG JDK_BUILD="28"
ARG JDK_ARCHIVE="openjdk-11+${JDK_BUILD}_linux-x64-musl_bin.tar.gz"
ARG OPENJDK11_ALPINE_URL=https://download.java.net/java/early_access/alpine/${JDK_BUILD}/binaries/${JDK_ARCHIVE}
#https://download.java.net/java/early_access/alpine/28/binaries/openjdk-11+28_linux-x64-musl_bin.tar.gz.sha256
ARG JDK_SHA=dd168445df94abf6fd2141a57c81ac5d57c1aeedf808169babffc4641e073a8e
ARG JAVA_HOME=/usr/lib/jvm/java-11-openjdk
ARG TZ=Asia/Jakarta

RUN apk add --update --no-cache tzdata && \
  cp "/usr/share/zoneinfo/$TZ" /etc/localtime && \
  echo "$TZ" > /etc/timezone && \
  apk del tzdata && \
  mkdir -p /usr/lib/jvm && \
  echo "downloading $OPENJDK11_ALPINE_URL, please wait..." && \
  wget -q -P /usr/lib/jvm $OPENJDK11_ALPINE_URL && \
  tar -xz -C /usr/lib/jvm -f /usr/lib/jvm/${JDK_ARCHIVE} && \
  mv /usr/lib/jvm/jdk-11 $JAVA_HOME && \
  echo "${JDK_SHA}  /usr/lib/jvm/${JDK_ARCHIVE}" | sha256sum -c - && \ 
  rm $JAVA_HOME/lib/src.zip && \
  rm /usr/lib/jvm/${JDK_ARCHIVE}

ENV JAVA_HOME=$JAVA_HOME \
    PATH=$PATH:${JAVA_HOME}/bin \
    SPRING_OUTPUT_ANSI_ENABLED=ALWAYS \
    JAVA_EXEC_SLEEP=0 \
    JAVA_OPTS="" \
    TZ="${TZ}"

WORKDIR /app
COPY entrypoint.sh entrypoint.sh
RUN chmod 755 entrypoint.sh

# https://docs.oracle.com/javase/10/tools/jshell.htm and https://en.wikipedia.org/wiki/JShell
CMD ["jshell"]
