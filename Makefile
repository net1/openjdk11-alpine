DOCKER_REPOSITORY ?= registry.gitlab.com/net1/openjdk11-alpine
DOCKER_TAG ?= $(shell cat version.txt)

DOCKER ?= docker
export DOCKER

.PHONY: all image push pull

image:
	$(DOCKER) build -t $(DOCKER_REPOSITORY):$(DOCKER_TAG) --build-arg version=${DOCKER_TAG} .

push: image
	$(DOCKER) push $(DOCKER_REPOSITORY):$(DOCKER_TAG)
	
pull:
	$(DOCKER) pull $(DOCKER_REPOSITORY):$(DOCKER_TAG)
